app.controller("listClients",["$scope","scopes","$timeout","$http",function($scope,scopes,$timeout,$http){

	scopes.store("listClients",$scope);

	$scope.data = {
		back : {},
		front : {
			get : {
				clients : {
					submit : function(){
						$scope.data.front.get.clients.active = true;
						io.socket.get("/Clients/Get",function(data){
							$scope.data.front.get.clients.active = false;
							$scope.data.front.list.elements = data.clients;
							$scope.$digest();
						});
					},
					active : true
				}
			},
			list : {
				filter : {
					id : generateRandomCode(25),
					value : "",
					maxLength : 100,
					active : false
				},
				elements : [],
				elementAction : function(element){
					$scope.data.front.windowModal.active = true;
					$scope.data.front.windowModal.client = angular.copy(element);
					$scope.data.front.windowModal.client.registered = $scope.data.front.windowModal.client.registered.substring(0,10);
					$scope.data.front.windowModal.client.registered = new Date($scope.data.front.windowModal.client.registered);
						
					$timeout(function(){
						var options = {
							center: {
								lat : $scope.data.front.windowModal.client.latitude,
								lng : $scope.data.front.windowModal.client.longitude
							},
							zoom : 4
						};
						var map = new google.maps.Map(document.getElementById($scope.data.front.windowModal.map.id),options);
					},0,false);
				}
			},
			windowModal : {
				client : {},
				map : {
					id : generateRandomCode(25)
				},
				active : false
			}
		}
	};

	$scope.data.front.get.clients.submit();

	$timeout(function(){
		$(".principal-container").css({
			visibility : "visible"
		})
	},0,false);

}]);