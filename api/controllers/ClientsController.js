/**
 * ClientsController
 *
 * @description :: Server-side logic for managing Clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var request = require("request");

module.exports = {
	clientsGet : function(req,res,next){
		try{
			var data = {
				front : req.params.all(),
				back : {},
				session : {}
			};

			request("http://socialh4ck.com/dev/jobs/test/frontend/data.json",function(error,response,body){
				if(!error && response.statusCode == 200){
					var clients = JSON.parse(body);
					res.json({clients:clients});
				}
			});

		}catch(error){
			console.log("ClientsController > clientsGet > 0: "+error);
		}
	}
};

